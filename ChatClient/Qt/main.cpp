/********************************************************************************
** Project: ChatClient
** File: Main.cpp
** Description: Main function
** Author: Lin Xin-Yu
** Created: Sun May 17 00:39:36 2009
** Blog: http://importcode.blogspot.com
**
********************************************************************************/

#include "chatclient.h"

#include <QtGui>
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ChatClient w;
    w.show();
    return a.exec();
}
