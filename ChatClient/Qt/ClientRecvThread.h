/********************************************************************************
** Project: ChatClient
** File: ClientRecvThread.h
** Description: A thread class for incoming message. This class just simply use
** 				QThread for thread porgramming.
** Author: Lin Xin-Yu
** Created: Sun May 17 00:39:36 2009
** Blog: http://importcode.blogspot.com
**
********************************************************************************/

#ifndef CLIEBTRECVTHREAD_H_
#define CLIENTRECVTHREAD_H_

#include <QThread>
#include "Client.h"
#include "ui_chatclient.h"

class ClientRecvThread:public QThread{
public:
	ClientRecvThread(Client* socket, Ui::ChatClientClass *ui);
	virtual ~ClientRecvThread();
	void QStringToChar(QString qs, char* s, int length);
	void stop();

protected:
	void run();

private:
	Client* socket;
	Ui::ChatClientClass *ui;
};

#endif /* QCLIENTRECVTHREAD_H_ */
