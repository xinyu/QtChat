#-------------------------------------------------
#
# Project created by QtCreator 2015-05-18T00:58:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ChatClient
TEMPLATE = app


SOURCES += main.cpp\
        chatclient.cpp \
    Client.cpp \
    ClientRecvThread.cpp

HEADERS  += chatclient.h \
    Client.h \
    ClientRecvThread.h

FORMS    += chatclient.ui
