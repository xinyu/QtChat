/********************************************************************************
** Project: ChatClient
** File: chatclient.cpp
** Description: A implementation for ChatClient class.
** Author: Lin Xin-Yu
** Created: Sun May 17 00:39:36 2009
** Blog: http://importcode.blogspot.com
**
********************************************************************************/

#include "chatclient.h"

ChatClient::ChatClient(QWidget *parent)
    : QWidget(parent)
{
	ui.setupUi(this);
	isConnecting = false;
	ui.pushButton_send->setEnabled(false);
	ui.comboBox_status->setEnabled(false);
	ui.lineEdit_input->setEnabled(false);
	ui.textEdit_output->setEnabled(false);
}

ChatClient::~ChatClient()
{
	if(isConnecting){
		disconnect();
	}
}


bool ChatClient::onConnectClicked(){
	if(isConnecting){
		disconnect();
	}else{
		connect();
	}

	return true;
}

bool ChatClient::onSendClicked(){
	int ret;
	char msg[256];

	QString qmsg = nickName+":"+ui.lineEdit_input->text();

	ui.textEdit_output->append(qmsg);
	ui.lineEdit_input->setText("");
	qmsg += "\n";
	QStringToChar(qmsg, msg, qmsg.length());

	try{
		ret = client->send(msg);
		if(ret <= 0 ) throw ret;
	}catch(int err){
		if(err == 0) ui.textEdit_output->append("Server is offline!\n");
		else if(err == -1) ui.textEdit_output->append("Fail to send message to Server!\n");
	}

	return true;
}

bool ChatClient::setNickName(){
	nickName = ui.lineEdit_name->text();
	if(nickName == ""){
		ui.textEdit_output->setText("Please give a name:");
		return false;
	}

	this->setWindowTitle("ChatClient - "+nickName);
	return true;
}

bool ChatClient::onNameChanged(){
	return setNickName();
}

void ChatClient::QStringToChar(QString qs, char* s, int length)
{
	int i=0;
	QByteArray bArr;

    bArr = qs.toLatin1();

	for(i=0; i<length; i++)
		s[i] = bArr[i];
	s[i]='\0';
}

void ChatClient::onStatusChanged(){
	int ret;
	char msg[256];

	QString qmsg = nickName+" is ["+ui.comboBox_status->currentText()+"]\n";
	QStringToChar(qmsg, msg, qmsg.length());
	try{
		ret = client->send(msg);
		if(ret <= 0 ) throw ret;
	}catch(int err){
		if(err == 0) ui.textEdit_output->append("Server is offline!\n");
		else if(err == -1) ui.textEdit_output->append("Fail to send message to Server!\n");
	}

}

bool ChatClient::connect(){
	if(nickName == ""){
		if(!setNickName()){
			return false;
		}
	}

	char ip[128];
	int port;
	bool ok;

	QString qip = ui.lineEdit_ip->text();
	QString qport = ui.lineEdit_port->text();

	if( qip.length() == 0 || qport.length() == 0 ){
		ui.textEdit_output->append("Please enter ip and port!");
		return false;
	}

	QStringToChar(qip, ip, qip.length());
	port = qport.toInt(&ok, 10);

	client = new Client();
	if(!client->connect(ip, port)){
		ui.textEdit_output->append("Fail to connect to Server!");
		return false;
	}
	
	isConnecting = true;

	ui.lineEdit_ip->setEnabled(false);
	ui.lineEdit_port->setEnabled(false);
	ui.pushButton_send->setEnabled(true);
	ui.comboBox_status->setEnabled(true);
	ui.comboBox_status->setCurrentIndex(0);
	ui.lineEdit_input->setEnabled(true);
	ui.textEdit_output->setEnabled(true);

	ui.pushButton_conn->setText("Disconnect");

	ui.textEdit_output->append("Connect to "+qip+":"+qport+"...");

	qthread = new ClientRecvThread(client, &ui);
	qthread->start();

	return true;
}

void ChatClient::disconnect(){
	char msg[256];
	QString qmsg = ":quit "+nickName+" is [offline]\n";

	QStringToChar(qmsg, msg, qmsg.length());
	client->send(msg);

	ui.pushButton_conn->setText("Connect");
	ui.pushButton_send->setEnabled(false);
	ui.lineEdit_ip->setEnabled(true);
	ui.lineEdit_port->setEnabled(true);
	ui.lineEdit_input->setEnabled(false);
	ui.textEdit_output->setEnabled(false);
	ui.comboBox_status->setEnabled(false);

	qthread->stop();
	delete qthread;

	client->close();
	delete client;
	
	isConnecting = false;

	ui.textEdit_output->append("disconnected");
}
