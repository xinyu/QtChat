/********************************************************************************
** Project: Client Socket
** File: Client.h
** Description: The class definition for C++ client socket implementation.
** Author: Lin Xin-Yu
** Created: Sun May 17 00:39:36 2009
** Blog: http://importcode.blogspot.com
**
********************************************************************************/


#ifndef _CLIENT_H_
#define _CLIENT_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>

#ifdef _WIN32
	#include <winsock.h>
#else
	#define SOCKET int
	#define INVALID_SOCKET -1
	#define SOCKET_ERROR -1

	#include <netdb.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
	#include <unistd.h>
	#include <fcntl.h>
#endif

#define MAX_BUF 1024

class Client{
	public:
		Client();
		Client(const char* ip, const int port);
		~Client();

	#ifdef _WIN32
		bool initWsa();
	#endif

		bool create();
		bool connect(const char* ip, const int port);

		int recv(char* buf);
		int send(const char* buf) const;

		int read(char* buf);
		int write(const char* buf) const;

		bool isValid() const;
		void setNonBlocked();

		int operator = (SOCKET socket);
		int operator << (const char* buf) const;
		int operator >> (char* buf);

		void close();

	private:
		int state;
		SOCKET socket;
};

#endif

