/********************************************************************************
** Project: ChatClient
** File: chatclient.h
** Description: The core class - ChatClient which describe the properties,
** 				methods, signals and slots for this ChatClient program.
** Author: Lin Xin-Yu
** Created: Sun May 17 00:39:36 2009
** Blog: http://importcode.blogspot.com
**
********************************************************************************/

#ifndef CHATCLIENT_H
#define CHATCLIENT_H

#include <QWidget>
#include "ui_chatclient.h"
#include "ClientRecvThread.h"
#include <QThread>
#include "Client.h"

class ChatClient : public QWidget
{
    Q_OBJECT

public:
    ChatClient(QWidget *parent = 0);
    ~ChatClient();
    void QStringToChar(QString qs, char* s, int length);
    bool setNickName();
    bool connect();
    void disconnect();

public slots:
	bool onConnectClicked();
	bool onSendClicked();
	bool onNameChanged();
	void onStatusChanged();

private:
	bool isConnecting;
	QString nickName;
    Ui::ChatClientClass ui;
    Client* client;
    ClientRecvThread* qthread;
};

#endif // CHATCLIENT_H
