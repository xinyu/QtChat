/********************************************************************************
** Project: ChatClient
** File: ClientRecvThread.cpp
** Description: Implementation for ClientRecvThread.h
** Author: Lin Xin-Yu
** Created: Sun May 17 00:39:36 2009
** Blog: http://importcode.blogspot.com
**
********************************************************************************/


#include "ClientRecvThread.h"

ClientRecvThread::ClientRecvThread(Client* socket, Ui::ChatClientClass *ui) {
	this->socket = socket;
	this->ui = ui;
}

ClientRecvThread::~ClientRecvThread() {

}

void ClientRecvThread::run(){
	int ret;
	char buf[MAX_BUF];

	while((ret = socket->recv(buf)) > 0){
		buf[ret-1]='\0';
		ui->textEdit_output->append(buf);
	}

	if(ret == 0){
		ui->textEdit_output->append("Server is offline or close the connection");
	}else{
		ui->textEdit_output->append("Fail to receive message from server!");
	}

	exit(0);
}

void ClientRecvThread::stop(){
	wait();
}

void ClientRecvThread::QStringToChar(QString qs, char* s, int length)
{
	int i=0;
	QByteArray bArr;

    bArr=qs.toLatin1();

	for(i=0;i<length;i++)
		s[i] = bArr[i];
	s[i]='\0';
}
